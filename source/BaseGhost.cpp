#pragma once

#include "BaseGhost.h"

BaseGhost::BaseGhost(void) : 
m_EatableSprite(0), m_EyesSprite(0), m_Eatable(false),  m_CornerX(0), m_CornerY(0), m_TargetX(0), m_TargetY(0), m_CurrentState(Patrol)
{
}

BaseGhost::BaseGhost(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY ) : 
m_EatableSprite(0), m_EyesSprite(0), m_Eatable(false), m_CornerX(cornerX), m_CornerY(cornerY), m_TargetX(0), m_TargetY(0), m_CurrentState(Patrol)
{
	m_PosX = xPos;
	m_PosY = yPos;
	m_PixelPosX = xPos * 16;
	m_PixelPosY = yPos * 16 - 8;
	m_Field = field;
	m_Pacman = pacman;
}

BaseGhost::~BaseGhost(void)
{
	delete m_EatableSprite;
	//delete m_EyesSprite;
}



void BaseGhost::Move()
{
	if(m_Sprite == 0)
	{
		return;
	}

	if(m_CurrentState == Frightened && (CL_System::get_time() - m_StartTime) / 1000.0f > 6.0f)
	{
		m_CurrentState = Patrol;
		m_Eatable = false;
		m_StartTime = 0;
	}



	// check if the ghost is able to change direction
	if(!m_Field->InHouse(m_PosX, m_PosY))
	{
		if((GetDirection() == None || ((m_PixelPosX - 8) % 16 == 0 && (m_PixelPosY - 8) % 16 == 0 && (m_Field->IsCrossing(m_PosX, m_PosY) || m_Field->IsEdge(m_PosX, m_PosY)))))
		{
			switch(m_CurrentState)
			{
			case Patrol:
				SetTarget(m_CornerX, m_CornerY);
				MoveToTarget();
				break;

			case Persuit:
				Chase(m_Pacman->GetPosX(), m_Pacman->GetPosY());
				break;

			case Frightened:
				Escape();
				break;
			}
		}
	}
	else
	{
		if(m_PixelPosX % 16 == 0 && (m_PixelPosY + 8) % 16 == 0)
		{
			m_LastDecision = None;
			SetTarget(m_CornerX, -m_CornerY);
			MoveToTarget();
		}
	}

	switch(GetDirection())
	{
		case None:
			return;

		case Up:
			if(m_Field->IsBlockedForGhost(m_PosX, m_PosY - 1))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosY--;

			if( ((m_PixelPosY + 8) % 16) == 0)
			{
				m_PosY = (m_PixelPosY + 8) / 16;
			}
			break;

		case Down:
			if(m_Field->IsBlocked(m_PosX, m_PosY + 1))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosY++;

			if( ((m_PixelPosY + 8) % 16) == 0)
			{
				m_PosY = (m_PixelPosY + 8) / 16;
			}
			break;

		case Left:
			if(m_Field->IsBlocked(m_PosX - 1, m_PosY))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosX--;

			if(((m_PixelPosX + 8) % 16) == 0)
			{
				m_PosX = (m_PixelPosX + 8) / 16;
				if(m_PosX < 0)
				{
					m_PosX = m_Field->GetSizeX() - 1;
					m_PixelPosX = m_PosX * 16;
				}
			}
			break;

		case Right:
			if(m_Field->IsBlocked(m_PosX + 1, m_PosY))
			{
				SetDirection(None);
				break;
			}

			m_PixelPosX++;

			if( ((m_PixelPosX + 8) % 16) == 0)
			{
				m_PosX = (m_PixelPosX + 8) / 16;
				if(m_PosX >= m_Field->GetSizeX() - 1)
				{
					m_PosX = 0;
					m_PixelPosX = 0;
				}
			}
			break;
	}
}

// Der Geist geht wenn er sich einem Ziel n�hert immer das l�ngere delta
// In dieser Funktion wird im Uhrzeigersinn �berpr�ft in welche Richtung 
// der Geist gehen kann
void BaseGhost::MoveToTarget()
{
	int deltaX, deltaY;

	deltaX = m_PosX - m_TargetX;
	deltaY = m_PosY - m_TargetY;

	if(abs(deltaX) < abs(deltaY)) //distanz in X richtung kleiner als in Y richtung
	{
		if(deltaY > 0)
		{
			if(!MoveVertical(true)) // nach oben testen
			{
				if(deltaX > 0) // in x richtung versuchen
				{
					if(!MoveHorizontal(true)) // links testen
					{
						if(!MoveVertical(false)) // unten testen
						{
							MoveHorizontal(false); // rechts testen
						}
					}
				}
				else
				{
					if(!MoveHorizontal(false)) // rechts testen
					{
						if(!MoveVertical(false)) // unten testen
						{
							MoveHorizontal(true); // links testen
						}
					}
				}
			}
		}
		else
		{
			if(!MoveVertical(false)) // nach unten testen
			{
				if(deltaX > 0) // wenn nicht nach unten, eine x richtung testen
				{
					if(!MoveHorizontal(true)) // links testen
					{
						if(!MoveVertical(true)) // nach oben testen
						{
							MoveHorizontal(false); //rechts testen
						}
					}
				}
				else
				{
					if(!MoveHorizontal(false)) // rechts testen
					{
						if(!MoveVertical(true)) // nach oben
						{
							MoveHorizontal(true); // nach links
						}
					}
				}
			}
		}
	}
	else
	{
		if(deltaX > 0)
		{
			if(!MoveHorizontal(true)) // links versuchen
			{
				if(deltaY > 0) // links nicht m�glich, y richtung testen
				{
					if(!MoveVertical(true)) // nach oben testen
					{
						if(!MoveVertical(false)) // rechts testen
						{
							MoveHorizontal(false); // nach unten testen
						}
					}
				}
				else // links nicht m�glich, y richtung testen
				{
					if(!MoveVertical(false)) // unten testen
					{
						if(!MoveVertical(true)) // oben testen
						{
							MoveHorizontal(false); // rechts versuchen
						}
					}
				}
			}
		}
		else
		{
			if(!MoveHorizontal(false)) // rechts versuchen
			{
				if(deltaY > 0) // rechts nicht m�glich, y richtung versuchen
				{
					if(!MoveVertical(true)) // nach oben testen
					{
						if(!MoveVertical(false)) // nach unten testen
						{
							MoveHorizontal(true); // links testen
						}
					}
				}
				else
				{
					if(!MoveVertical(false)) // nach unten testen
					{
						if(!MoveVertical(true)) // nach oben testen
						{
							MoveHorizontal(true); // links testen
						}
					}
				}
			}
		}
	}
}

bool BaseGhost::MoveHorizontal(bool left)
{
	if(left)
	{
		if(!m_Field->IsBlocked(m_PosX - 1, m_PosY) && m_LastDecision != Right)
		{
			if(SetDirection(Left))
			{
				m_LastDecision = Left;
				return true;
			}
		}
	}
	else
	{
		if(!m_Field->IsBlocked(m_PosX + 1, m_PosY) && m_LastDecision != Left)
		{
			if(SetDirection(Right))
			{
				m_LastDecision = Right;
				return true;
			}
		}
	}

	SetDirection(None);
	return false;
}

bool BaseGhost::MoveVertical(bool up)
{
	if(up)
	{
		if(!m_Field->IsBlockedForGhost(m_PosX, m_PosY - 1) && m_LastDecision != Down)
		{
			if(SetDirection(Up))
			{
				m_LastDecision = Up;
				return true;
			}
		}
	}
	else
	{
		if(!m_Field->IsBlocked(m_PosX, m_PosY + 1)  && m_LastDecision != Up)
		{
			if(SetDirection(Down))
			{
				m_LastDecision = Down;
				return true;
			}
		}
	}

	return false;
}

void BaseGhost::Render(CL_GraphicContext gc)
{
	if(!m_EatableSprite)
	{
		CL_ResourceManager resources("resources/resources.xml");
		m_EatableSprite = new CL_Sprite(gc, "ghost_eatable", &resources);
	}

	m_EatableSprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
	m_EatableSprite->update();
}

// Diese Methode sollte die Augen so rendern das die Geister immer 
// in die Richung sehen in die sie gehen 
// Funktioniert jedoch nicht bekomme keine Direction zur�ck
void BaseGhost::RenderEyes(CL_GraphicContext gc)
{
		if(GetDirection()==Up)
		{	if(!m_EyesSprite_Up)
			{
				CL_ResourceManager resources("resources/resources.xml");
				m_EyesSprite_Up= new CL_Sprite(gc, "ghost_eyes_up", &resources);
				m_EyesSprite_Up->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
				m_EyesSprite_Up->update();
				delete m_EyesSprite_Up;
			}
		}
		else if(GetDirection()==Right)
		{	if(!m_EyesSprite_Right)
			{
				CL_ResourceManager resources("resources/resources.xml");
				m_EyesSprite_Right= new CL_Sprite(gc, "ghost_eyes_right", &resources);
				m_EyesSprite_Right->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
				m_EyesSprite_Right->update();
				delete m_EyesSprite_Right;
			}
		}
		else if(GetDirection()==Down)
		{	if(!m_EyesSprite_Down)
			{
				CL_ResourceManager resources("resources/resources.xml");
				m_EyesSprite_Down= new CL_Sprite(gc, "ghost_eyes_down", &resources);
				m_EyesSprite_Down->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
				m_EyesSprite_Down->update();
				delete m_EyesSprite_Down;
			}
		}
		else if(GetDirection()==Left)
		{	if(!m_EyesSprite_Left)
			{
				CL_ResourceManager resources("resources/resources.xml");
				m_EyesSprite_Left= new CL_Sprite(gc, "ghost_eyes_left", &resources);
				m_EyesSprite_Left->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
				m_EyesSprite_Left->update();
				delete m_EyesSprite_Left;
			}
		}
		else if(GetDirection()==None)
		{
			if(!m_EyesSprite_Left)
			{
				CL_ResourceManager resources("resources/resources.xml");
				m_EyesSprite= new CL_Sprite(gc, "ghost_eyes_up", &resources);
				m_EyesSprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
				m_EyesSprite->update();
			}
		}

	//m_EyesSprite->draw(gc, static_cast<float>(m_TargetX * 16), static_cast<float>(m_TargetY * 16));
	
}

void BaseGhost::SetTarget( int targetX,  int targetY)
{
	m_TargetX = targetX;
	m_TargetY = targetY;
}

void BaseGhost::SetState(EGhostState state)
{
	if(state != EGhostState::Frightened && m_CurrentState != EGhostState::Frightened)
	{
		m_CurrentState = state;
		return;
	}

	if(state == EGhostState::Frightened)
	{
		m_StartTime = CL_System::get_time();
		m_CurrentState = Frightened;

		switch(GetDirection())
		{
		case Up:
			if(SetDirection(Down))
			{
				m_LastDecision = Down;
			}
			break;
		case Down:
			if(SetDirection(Up))
			{
				m_LastDecision = Up;
			}
			break;
		case Left:
			if(SetDirection(Right))
			{
				m_LastDecision = Right;
			}
			break;
		case Right:
			if(SetDirection(Left))
			{
				m_LastDecision = Left;
			}
			break;
		}

		m_Eatable = true;
	}
}

void BaseGhost::Escape()
{
	srand ( time(NULL) );
	int newDirection = rand() % 4 + 1;
	bool directionSet = false;

	while(!directionSet)
	{
		// m_LastDecision- Variable kann von Move-Methode �bernommen werden, damit die KI nicht den selben Weg zur�ck geht, den sie gekommen ist
		if(static_cast<EDirectionType>(newDirection) != m_LastDecision) //weg zur�ck nicht nehmen
		{
			if(SetDirection(static_cast<EDirectionType>(newDirection))) //neuen weg testen
			{
				m_LastDecision = static_cast<EDirectionType>(newDirection);
				directionSet = true;
			}
		}

		newDirection = newDirection % 4 + 1;
	}
}