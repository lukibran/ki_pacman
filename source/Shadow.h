#pragma once

#include "BaseGhost.h"

class Shadow : public BaseGhost
{
public:
	Shadow(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Shadow(void);
	
	virtual void Render(CL_GraphicContext gc);
	virtual void Chase(int pacmanX, int pacmanY);

private:
	CL_Sprite* m_EatableSprite;
};

