#pragma once

#include <math.h>

#include "BaseGhost.h"

class Pokey : public BaseGhost
{
public:
	Pokey(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Pokey(void);
	
	virtual void Render(CL_GraphicContext gc);
	virtual void Chase(int pacmanX, int pacmanY);

private:
	CL_Sprite* m_EatableSprite;
};

