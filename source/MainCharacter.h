#pragma once
#include "../ACharacter.h"
#include "../precomp.h"

#define BUFFERTIME  32

class MainCharacter : public ACharacter
{
public:
	MainCharacter(CL_Sprite* spriteUp, CL_Sprite* spriteRight, CL_Sprite* spriteDown, CL_Sprite* spriteLeft, CL_InputDevice keyboard, Field* field);
	~MainCharacter(void);
	void Update();
	void Render(CL_GraphicContext gc);
	
private:
	CL_Sprite* m_SpriteUp;
	CL_Sprite* m_SpriteRight;
	CL_Sprite* m_SpriteDown;
	CL_Sprite* m_SpriteLeft;
	CL_InputDevice m_Keyboard;
	int m_PressedKeyBuffer;
	short m_CurrentBufferStep;

	virtual void Move();
	virtual bool SetDirection(EDirectionType);
};