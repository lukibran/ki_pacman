#include "MainCharacter.h"

MainCharacter::MainCharacter(CL_Sprite* spriteUp, CL_Sprite* spriteRight, CL_Sprite* spriteDown, CL_Sprite* spriteLeft, CL_InputDevice keyboard, Field* field)
{		
	m_Sprite = spriteDown;
	m_SpriteUp = spriteUp;
	m_SpriteRight = spriteRight;
	m_SpriteDown = spriteDown;
	m_SpriteLeft = spriteLeft;
	m_Keyboard = keyboard;
	m_Field = field;
	m_PressedKeyBuffer = 0;
	m_CurrentBufferStep = 0;
	m_PosX = 13;
	m_PosY = 23;
	//m_PosX = 2;
	//m_PosY = 1;
	m_PixelPosX = m_PosX * 16;
	m_PixelPosY = m_PosY * 16 - 8;
}

MainCharacter::~MainCharacter(void)
{
}


void MainCharacter::Update()
{
	if(m_CurrentBufferStep > 0)
	{
		--m_CurrentBufferStep;
	}
	else
	{
		m_PressedKeyBuffer = 0;
	}

	if(m_Keyboard.get_keycode(CL_KEY_W) || m_Keyboard.get_keycode(CL_KEY_UP))
	{
		m_PressedKeyBuffer = CL_KEY_W;
		m_CurrentBufferStep = BUFFERTIME;
	}
	else if(m_Keyboard.get_keycode(CL_KEY_A) || m_Keyboard.get_keycode(CL_KEY_LEFT))
	{
		m_PressedKeyBuffer = CL_KEY_A;
		m_CurrentBufferStep = BUFFERTIME;
	}	
	else if(m_Keyboard.get_keycode(CL_KEY_S) || m_Keyboard.get_keycode(CL_KEY_DOWN))
	{
		m_PressedKeyBuffer = CL_KEY_S;
		m_CurrentBufferStep = BUFFERTIME;
	}	
	else if(m_Keyboard.get_keycode(CL_KEY_D) || m_Keyboard.get_keycode(CL_KEY_RIGHT))
	{
		m_PressedKeyBuffer = CL_KEY_D;
		m_CurrentBufferStep = BUFFERTIME;
	}

	if(m_CurrentBufferStep > 0)
	{
		if(m_PressedKeyBuffer == CL_KEY_W && !m_Field->IsBlocked(m_PosX, m_PosY - 1))
		{
			if(SetDirection(Up))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}	
		else if(m_PressedKeyBuffer == CL_KEY_A && !m_Field->IsBlocked(m_PosX - 1, m_PosY))
		{
			if(SetDirection(Left))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}	
		else if(m_PressedKeyBuffer == CL_KEY_S && !m_Field->IsBlocked(m_PosX, m_PosY + 1))
		{
			if(SetDirection(Down))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}	
		else if(m_PressedKeyBuffer == CL_KEY_D && !m_Field->IsBlocked(m_PosX + 1, m_PosY))
		{
			if(SetDirection(Right))
			{
				m_PressedKeyBuffer = 0;
				m_CurrentBufferStep = 0;
			}
		}
	}

	ACharacter::Update();
}

void MainCharacter::Move()
{
	ACharacter::Move();

	char fieldChar = m_Field->GetField(m_PosX, m_PosY);

	if(fieldChar == '.' || fieldChar == '*')
	{
		m_Field->EatField(m_PosX, m_PosY);
	}
}

bool MainCharacter::SetDirection(EDirectionType directionType)
{
	bool value = ACharacter::SetDirection(directionType);

	if(value)
	{
		switch(GetDirection())
		{
		case Up:
			m_Sprite = m_SpriteUp;
			break;

		case Right:
			m_Sprite = m_SpriteRight;
			break;

		case Down:
			m_Sprite = m_SpriteDown;
			break;

		case Left:
			m_Sprite = m_SpriteLeft;
			break;
		}
	}

	return value;
}

void MainCharacter::Render(CL_GraphicContext gc)
{
	if(GetDirection() != None)
	{
		m_Sprite->update();
	}

	m_Sprite->draw(gc, (float)m_PixelPosX, (float)m_PixelPosY);
}