#pragma once

#include "precomp.h"
#include "Field.h"

enum EDirectionType 
{
	None = 0,
	Up = 1,
	Right = 2,
	Down = 3,
	Left = 4
};

class ACharacter
{
public:

	ACharacter(void);
	~ACharacter(void);

	virtual void Update();
	virtual void Render(CL_GraphicContext gc) = 0;

	virtual bool SetDirection(EDirectionType);
	EDirectionType GetDirection() const;

	short GetPosX();
	short GetPosY();
	void is_dead();
	int getLives();
	void setLives(int l);
	


protected:
	short m_PosX;
	short m_PosY;

	CL_Sprite* m_Sprite;

	int m_PixelPosX;
	int m_PixelPosY;
	
	Field* m_Field;

	virtual void Move();
	int lives;
private:
	EDirectionType m_DirectionType;
};