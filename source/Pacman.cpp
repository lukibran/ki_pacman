#include "Pacman.h"

float timer = 0.0f;
float fps_timer = 0.0f;
unsigned short round = 0;
#define FPS_LIMIT 100

Pacman::Pacman(void) : m_Field(NULL), m_Pacman(NULL), m_Shadow(NULL), m_Speedy(NULL), m_Bashful(NULL), m_Pokey(NULL)
{
}

Pacman::~Pacman(void)
{
	delete m_Field;
	delete m_Pacman;
	delete m_Shadow;
	delete m_Speedy;
	delete m_Bashful;
	delete m_Pokey;
}

int Pacman::start(const std::vector<CL_String> &args)
{
	quit = false;

	CL_DisplayWindowDescription desc;
	desc.set_title("Pacman");
	desc.set_size(CL_Size(450, 550), true);
	desc.set_allow_resize(true);

	CL_DisplayWindow window(desc);
	CL_InputContext ic = window.get_ic();
	CL_InputDevice keyboard = ic.get_keyboard();
	CL_InputDevice mouse = ic.get_mouse();

	CL_Slot slot_quit = window.sig_window_close().connect(this, &Pacman::on_window_close);
	CL_Slot slot_input_up = (window.get_ic().get_keyboard()).sig_key_up().connect(this, &Pacman::on_input_up);
	CL_GraphicContext gc = window.get_gc();

	 int last_time = CL_System::get_time();

	CL_ResourceManager resources("resources/resources.xml");

	CL_Sprite Button(gc, "resources/StartButton.jpg");
	CL_Sprite Titel(gc, "resources/TitelScreen.jpg");
	CL_Sprite Lives(gc, "resources/Lives.jpg");
	CL_Sprite LivesText(gc, "resources/LivesText.jpg");
	CL_Sprite Score(gc, "resources/Score.jpg");

	m_Field = new Field(gc, resources);

	 m_Pacman = new MainCharacter(new CL_Sprite(gc, "pacman_up", &resources),
         new CL_Sprite(gc, "pacman_right", &resources),
         new CL_Sprite(gc, "pacman_down", &resources),
         new CL_Sprite(gc, "pacman_left", &resources), keyboard, m_Field);

	m_Shadow = new Shadow(m_Pacman, m_Field, 13, 11, 24, -2);
	m_Speedy = new Speedy(m_Pacman, m_Field, 13, 13, 2, -2);
	m_Bashful = new Bashful(m_Pacman, m_Shadow, m_Field, 11, 11, 25, 32);
	m_Pokey = new Pokey(m_Pacman, m_Field, 15, 11, 1, 32);

	m_Pacman->setLives(0);

	
	while (!quit)
	{
		int current_time = CL_System::get_time();
		float time_delta_ms = static_cast<float>(current_time - last_time);
		timer += time_delta_ms / 500.0f;
		fps_timer += time_delta_ms / 1000.0f;
		last_time = current_time;

		if(m_Pacman->getLives() == 0)
		{
			//Startfenster Objekte Zeichnen
			gc.clear(CL_Colorf(0.0f,0.0f,0.0f));
			Button.draw(gc, 140, 320);
			Titel.draw(gc,-10,100);
			
			//Bei 0 Leben wir eine Game Over Nachricht angezeigt
			if(n_round== 0)
			{
				gc.clear(CL_Colorf(0.0f,0.0f,0.0f));
				CL_Font font(gc,"Tahoma",40);
				font.draw_text(gc,130,280,"GAME OVER");
			}
			
			//Bei MausClick oder Enter werden die Leben auf 3 angehoben
			bool mouse_down = mouse.get_keycode(CL_MOUSE_LEFT);
			if(mouse_down)
			{
				m_Pacman->setLives(3);
				n_round = 0;
			}
			bool enter_down = keyboard.get_keycode(CL_KEY_ENTER);
			if(enter_down)
			{
				m_Pacman->setLives(3);
				n_round = 0;
			}
			window.flip(1);
			CL_KeepAlive::process(0);
		}
		else
		{	//5 sekunden weglaufen / patrollieren 15 sekunden verfolgen
			if((timer > 5.0f) && (round % 2 == 0)) 
			{
				timer = 0.0f;
				round++;
				static_cast<BaseGhost*>(m_Shadow)->SetState(BaseGhost::EGhostState::Persuit);
				static_cast<BaseGhost*>(m_Speedy)->SetState(BaseGhost::EGhostState::Persuit);
				static_cast<BaseGhost*>(m_Bashful)->SetState(BaseGhost::EGhostState::Persuit);
				static_cast<BaseGhost*>(m_Pokey)->SetState(BaseGhost::EGhostState::Persuit);
			}
			// danach patrollieren
			else if((timer > 20.0f) && (round % 2 == 1)) 
			{
				timer = 0.0f;
				round++;
				static_cast<BaseGhost*>(m_Shadow)->SetState(BaseGhost::EGhostState::Patrol);
				static_cast<BaseGhost*>(m_Speedy)->SetState(BaseGhost::EGhostState::Patrol);
				static_cast<BaseGhost*>(m_Bashful)->SetState(BaseGhost::EGhostState::Patrol);
				static_cast<BaseGhost*>(m_Pokey)->SetState(BaseGhost::EGhostState::Patrol);
			}

			if(m_Field->GetBigDotEaten())
			{
				static_cast<BaseGhost*>(m_Shadow)->SetState(BaseGhost::EGhostState::Frightened);
				static_cast<BaseGhost*>(m_Speedy)->SetState(BaseGhost::EGhostState::Frightened);
				static_cast<BaseGhost*>(m_Bashful)->SetState(BaseGhost::EGhostState::Frightened);
				static_cast<BaseGhost*>(m_Pokey)->SetState(BaseGhost::EGhostState::Frightened);
			}

			if(fps_timer > (1.0f / FPS_LIMIT))
			{
				fps_timer = 0.0f;
				m_Pacman->Update();
				m_Shadow->Update();
				m_Speedy->Update();
				m_Bashful->Update();
				m_Pokey->Update();
			}

			gc.clear(CL_Colorf(0.0f,0.0f,0.0f));

			m_Field->DrawField();
			m_Pacman->Render(gc);
			m_Shadow->Render(gc);
			m_Speedy->Render(gc);
			m_Bashful->Render(gc);
			m_Pokey->Render(gc);
		
			//Score Ausgabe und Berechnung
			Score.draw(gc, 10,510);
			n_points = m_Field->GetDotsEaten();
			n_points = n_points * 10;
			//if(m_Field->GetBigDotEaten()==true)
			//	n_points += 50;
			CL_Font font(gc,"Tahoma",40);
			itoa(n_points,c_points,10);
			font.draw_text(gc,120,535,c_points);
			LivesText.draw(gc,255,510);

			//Leben Ausgeben und Verringern bei Tod
			if(m_Pacman->getLives() == 3)
			{
				Lives.draw(gc,330,505);
				Lives.draw(gc,360,505);
				Lives.draw(gc,390,505);
				n_round = 2;
			}
			if(m_Pacman->getLives() == 2)
			{
				Lives.draw(gc,330,505);
				Lives.draw(gc,360,505);
				if(n_round== 2)
				{   //Damit bei Tod Geister wieder vom Start beginnen
					m_Shadow = new Shadow(m_Pacman, m_Field, 13, 11, 24, -2);
					m_Speedy = new Speedy(m_Pacman, m_Field, 13, 13, 2, -2);
					m_Bashful = new Bashful(m_Pacman, m_Shadow, m_Field, 11, 11, 25, 32);
					m_Pokey = new Pokey(m_Pacman, m_Field, 15, 11, 1, 32);
					n_round = 1;
				}
				
			}
			if(m_Pacman->getLives() == 1)
			{
				Lives.draw(gc,330,505);
				if(n_round == 1)
				{
					m_Shadow = new Shadow(m_Pacman, m_Field, 13, 11, 24, -2);
					m_Speedy = new Speedy(m_Pacman, m_Field, 13, 13, 2, -2);
					m_Bashful = new Bashful(m_Pacman, m_Shadow, m_Field, 11, 11, 25, 32);
					m_Pokey = new Pokey(m_Pacman, m_Field, 15, 11, 1, 32);
					n_round = 0;
				}
			}

			window.flip(1);
			CL_KeepAlive::process(0);
		}
	}

	return 0;
}

void Pacman::on_input_up(const CL_InputEvent &key, const CL_InputState &state)
{
	if(key.id == CL_KEY_ESCAPE)
	{
		quit = true;
	}
}

void Pacman::on_window_close()
{
	quit = true;
}