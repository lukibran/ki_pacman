#pragma once

#include "precomp.h"
#include "Field.h"
#include "pacman\MainCharacter.h"
#include "ghosts\BaseGhost.h"
#include "ghosts\Shadow.h"
#include "ghosts\Speedy.h"
#include "ghosts\Bashful.h"
#include "ghosts\Pokey.h"

class Pacman
{
public:
	Pacman(void);
	~Pacman(void);
	int start(const std::vector<CL_String> &args);

private:
	Field* m_Field;
	MainCharacter* m_Pacman;
	Shadow* m_Shadow;
	Speedy* m_Speedy;
	Bashful* m_Bashful;
	Pokey* m_Pokey;
	int n_round;
	int n_points;
	char c_points[50];

	void on_input_up(const CL_InputEvent &key, const CL_InputState &state);
	void on_window_close();
	bool quit;
};

