#include "Shadow.h"

Shadow::Shadow(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
	/*
	m_PosX = xPos;
	m_PosY = yPos;
	m_PixelPosX = xPos * 32;
	m_PixelPosY = yPos * 32;
	m_PixelPosX = xPos * m_Sprite->get_width();
	m_PixelPosY = yPos * m_Sprite->get_height();*/
}

Shadow::~Shadow(void)
{
	delete m_Sprite;
}

void Shadow::Render(CL_GraphicContext gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			CL_ResourceManager resources("resources/resources.xml");
			m_Sprite = new CL_Sprite(gc, "shadow", &resources);
		}

		m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
		m_Sprite->update();

		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}
//Haupt KI von Shadow
void Shadow::Chase(int pacmanX, int pacmanY)
{
	m_TargetX = pacmanX;
	m_TargetY = pacmanY;
	SetTarget(m_TargetX,m_TargetY);
	MoveToTarget();
	
	//Überprüfung auf Zusammentreffen mit Pacman
	if ( m_PosX == m_Pacman->GetPosX() && m_PosY == m_Pacman->GetPosY() && m_CurrentState == Persuit || m_CurrentState == Patrol)
	{
		m_Pacman->is_dead();
	}
}
