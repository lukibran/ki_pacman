#include "Speedy.h"

Speedy::Speedy(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
}

Speedy::~Speedy(void)
{
	delete m_Sprite;
	//delete m_EyesSprite;
}

void Speedy::Render(CL_GraphicContext gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			CL_ResourceManager resources("resources/resources.xml");
			m_Sprite = new CL_Sprite(gc, "speedy", &resources);
		}

		m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
		m_Sprite->update();

		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}

// HauptKI von Speedy
void Speedy::Chase(int pacmanX, int pacmanY)
{
	switch(m_Pacman->GetDirection())
	{
		case Up:
			m_TargetX = pacmanX;
			m_TargetY = pacmanY - 4;
			SetTarget(m_TargetX,m_TargetY);
			break;
		case Right:
			m_TargetX = pacmanX + 4;
			m_TargetY = pacmanY;
			SetTarget(m_TargetX,m_TargetY);
			break;
		case Down:
			m_TargetX = pacmanX;
			m_TargetY = pacmanY + 4;
			SetTarget(m_TargetX,m_TargetY);
			break;
		case Left:
			m_TargetX = pacmanX - 4;
			m_TargetY = pacmanY;
			SetTarget(m_TargetX,m_TargetY);
			break;
	}
	
	MoveToTarget();

	//Überprüfung auf Zusammentreffen mit Pacman
	if ( m_PosX == m_Pacman->GetPosX() && m_PosY == m_Pacman->GetPosY() && m_CurrentState == Persuit || m_CurrentState == Patrol)
	{
		m_Pacman->is_dead();
	}
}
