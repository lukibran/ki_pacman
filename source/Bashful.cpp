#include "Bashful.h"

Bashful::Bashful(ACharacter* pacman, ACharacter* shadow, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
	m_Shadow = shadow;
}

Bashful::~Bashful(void)
{
	delete m_Sprite;
}

void Bashful::Render(CL_GraphicContext gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			CL_ResourceManager resources("resources/resources.xml");
			m_Sprite = new CL_Sprite(gc, "bashful", &resources);
		}

		m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
		m_Sprite->update();

		
		BaseGhost::RenderEyes(gc);
	}
	else
	{
		BaseGhost::Render(gc);
	}
}

// Haupt KI von Bashful
void Bashful::Chase(int pacmanX, int pacmanY)
{
	if(m_Pacman->GetDirection()==1)
	{
		m_TargetX  = pacmanX;
		m_TargetY = pacmanY - 2;
	}
	else if(m_Pacman->GetDirection()==2)
	{
		m_TargetX = pacmanX + 2;
		m_TargetY = pacmanY;
	}
	else if(m_Pacman->GetDirection()==3)
	{
		m_TargetX = pacmanX;
		m_TargetY = pacmanY + 2;
	}
	else if(m_Pacman->GetDirection()==4)
	{
		m_TargetX = pacmanX-2;
		m_TargetY = pacmanY;
	}
	else
	{
		m_TargetX = pacmanX;
		m_TargetY = pacmanY;
	}

	deltaX = m_Shadow->GetPosX() - m_TargetX;
	deltaY = m_Shadow->GetPosY() - m_TargetY;

	int dX = m_PosX - m_TargetX;
	int dY = m_PosY - m_TargetY;
	
	short m_BashTargetX = (m_Shadow->GetPosX() - (deltaX * 2));
	short m_BashTargetY = (m_Shadow->GetPosY() - (deltaY * 2));

	SetTarget(m_BashTargetX,m_BashTargetY);
	MoveToTarget();
	
	/*
	if(sqrt(dX*dX+dY*dY)<=1 && (m_CurrentState == Persuit || m_CurrentState == Patrol))
		m_Pacman->is_dead();
	*/

	//Überprüfung auf Zusammentreffen mit Pacman
	if ( m_PosX == m_Pacman->GetPosX() && m_PosY == m_Pacman->GetPosY() && (m_CurrentState == Persuit || m_CurrentState == Patrol))
	{
		m_Pacman->is_dead();
	}
	

}