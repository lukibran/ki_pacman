#include "Pokey.h"

Pokey::Pokey(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY) : BaseGhost(pacman, field, xPos, yPos, cornerX, cornerY)
{
}

Pokey::~Pokey(void)
{
	delete m_Sprite;
}

void Pokey::Render(CL_GraphicContext gc)
{
	if(!m_Eatable)
	{
		if(!m_Sprite)
		{
			CL_ResourceManager resources("resources/resources.xml");
			m_Sprite = new CL_Sprite(gc, "pokey", &resources);
		}

		m_Sprite->draw(gc, static_cast<float>(m_PixelPosX), static_cast<float>(m_PixelPosY));
		m_Sprite->update();

		BaseGhost::RenderEyes(gc);

	}
	else
	{
		BaseGhost::Render(gc);
	}
}
// Haupt KI von Pokey
void Pokey::Chase(int pacmanX, int pacmanY)
{
	m_TargetX = pacmanX;
	m_TargetY = pacmanY;
	deltaX = m_PosX - m_TargetX;
	deltaY = m_PosY - m_TargetY;

	if(sqrt(deltaX*deltaX+deltaY*deltaY)>8)
	{
		SetTarget(m_TargetX,m_TargetY);
		MoveToTarget();
	}
	else
	{
		SetTarget(m_CornerX,m_CornerY);
		MoveToTarget();
	}
	//Überprüfung auf Zusammentreffen mit Pacman
	if ( m_PosX == m_Pacman->GetPosX() && m_PosY == m_Pacman->GetPosY() && m_CurrentState == Persuit ||m_CurrentState == Patrol)
	{
		m_Pacman->is_dead();
	}
}
