#pragma once

#include "ACharacter.h"

ACharacter::ACharacter(void) :
m_PosX(1), m_PosY(1), m_Sprite(0), m_PixelPosX(32), m_PixelPosY(32), m_DirectionType(None)
{

}

ACharacter::~ACharacter(void)
{
}

void ACharacter::is_dead()
{
	lives--;
}

int ACharacter::getLives()
{
	return lives;
}

void ACharacter::setLives(int l)
{
	lives = l;
}

void ACharacter::Move()
{
	if(m_Sprite == 0)
	{
		return;
	}

	switch(m_DirectionType)
	{
	case None:
		return;

	case Up:
		if(m_Field->IsBlocked(m_PosX, m_PosY - 1))
		{
			m_DirectionType = None;
			break;
		}
		m_PixelPosY--;
		if( ((m_PixelPosY + 8) % 16) == 0)
		{
			m_PosY = (m_PixelPosY + 8) / 16;
		}
		break;

	case Down:
		if(m_Field->IsBlocked(m_PosX, m_PosY + 1))
		{
			m_DirectionType = None;
			break;
		}
		m_PixelPosY++;
		if( ((m_PixelPosY + 8) % 16) == 0)
		{
			m_PosY = (m_PixelPosY + 8) / 16;
		}
		break;

	case Left:
		if(m_Field->IsBlocked(m_PosX - 1, m_PosY))
		{
			m_DirectionType = None;
			break;
		}
		m_PixelPosX--;
		if(((m_PixelPosX + 8) % 16) == 0)
		{
			m_PosX = (m_PixelPosX + 8) / 16;
			if(m_PosX < 0)
			{
				m_PosX = m_Field->GetSizeX() - 1;
				m_PixelPosX = m_PosX * 16;
			}
		}
		break;

	case Right:
		if(m_Field->IsBlocked(m_PosX + 1, m_PosY))
		{
			m_DirectionType = None;
			break;
		}
		m_PixelPosX++;
		if( ((m_PixelPosX + 8) % 16) == 0)
		{
			m_PosX = (m_PixelPosX + 8) / 16;
			if(m_PosX >= m_Field->GetSizeX() - 1)
			{
				m_PosX = 0;
				m_PixelPosX = 0;
			}
		}
		break;
	}
}

void ACharacter::Update()
{
	Move();
}

bool ACharacter::SetDirection(EDirectionType directionType)
{
	if(directionType != None)
	{
		if(!m_Field->InHouse(m_PosX, m_PosY))
		{
			if((directionType == Left || directionType == Right) && (m_PixelPosY + 8) % 16 == 0)
			{
				m_DirectionType = directionType;
				return true;
			}

			if((directionType == Up || directionType == Down) && (m_PixelPosX - 8) % 16 == 0)
			{
				m_DirectionType = directionType;
				return true;
			}
		}
		else
		{
			if((directionType == Left || directionType == Right) && m_PixelPosY % 16 == 0)
			{
				m_DirectionType = directionType;
				return true;
			}

			if((directionType == Up || directionType == Down) && m_PixelPosX % 16 == 0)
			{
				m_DirectionType = directionType;
				return true;
			}
		}
		return false;
	}
	else
	{
		m_DirectionType = directionType;
		return true;
	}
}

short ACharacter::GetPosX()
{
	return m_PosX;
}

short ACharacter::GetPosY()
{
	return m_PosY;
}

EDirectionType ACharacter::GetDirection() const
{
	return m_DirectionType;
}