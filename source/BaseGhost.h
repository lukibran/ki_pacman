#pragma once
#include <math.h>
#include <time.h>
#include <random>

#include "../ACharacter.h"

class BaseGhost : public ACharacter
{

private:
	CL_Sprite* m_EatableSprite;
	CL_Sprite* m_EyesSprite_Up;
	CL_Sprite* m_EyesSprite_Down;
	CL_Sprite* m_EyesSprite_Left;
	CL_Sprite* m_EyesSprite_Right;
	CL_Sprite* m_EyesSprite;
	
	int m_StartTime;

public:
	enum EGhostState
	{
		Patrol = 0,
		Persuit = 1,
		Frightened = 2
	};

	BaseGhost(void);
	BaseGhost(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~BaseGhost(void);

	virtual void Move();
	virtual void Render(CL_GraphicContext gc) = 0;
	virtual void Chase(int pacmanX, int pacmanY) = 0;
	void Escape();
	void SetTarget( int targetX,  int targetY);
	void SetState(EGhostState state);

protected:
	bool m_Eatable;
	short m_CornerX;		// the home corner of the ghost
	short m_CornerY;
	short m_TargetX;		// the current target of the ghost
	short m_TargetY;
	EGhostState m_CurrentState;
	EDirectionType m_LastDecision;
	virtual void RenderEyes(CL_GraphicContext gc);
	void MoveToTarget();
	bool MoveHorizontal(bool left);
	bool MoveVertical(bool up);
	ACharacter* m_Pacman;
	int deltaX, deltaY;
};