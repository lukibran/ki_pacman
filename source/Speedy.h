#pragma once

#include "BaseGhost.h"

class Speedy : public BaseGhost
{
public:
	Speedy(ACharacter* pacman, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Speedy(void);

	virtual void Render(CL_GraphicContext gc);
	virtual void Chase(int pacmanX, int pacmanY);

private:
	CL_Sprite* m_EatableSprite;
};