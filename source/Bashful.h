#pragma once

#include "BaseGhost.h"

class Bashful : public BaseGhost
{
private:
	ACharacter* m_Shadow;
	

public:
	Bashful(ACharacter* pacman, ACharacter* shadow, Field* field,  short xPos,  short yPos,  short cornerX,  short cornerY);
	~Bashful(void);
	
	virtual void Render(CL_GraphicContext gc);
	virtual void Chase(int pacmanX, int pacmanY);
};

